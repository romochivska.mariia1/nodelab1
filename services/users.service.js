const mockData = require('../helpers/mock-data');

function _generateId() {
    const crypto = require("crypto");
    return crypto.randomBytes(16).toString("hex");
}

async function createGrade(gradeData) {
    const newGrade = { id: _generateId(), ...gradeData };
    mockData.grades.push(newGrade);
    return newGrade;
}

async function getGrades() {
    return mockData.grades;
}

async function getGradeById(gradeId) {
    return mockData.grades.find(grade => grade.id === gradeId);
}

async function updateGrade(gradeId, updatedData) {
    const index = mockData.grades.findIndex(grade => grade.id === gradeId);
    if (index === -1) return;
    mockData.grades[index] = { ...mockData.grades[index], ...updatedData };
}

async function deleteGrade(gradeId) {
    mockData.grades = mockData.grades.filter(grade => grade.id !== gradeId);
}

module.exports = {
    createGrade,
    getGrades,
    getGradeById,
    updateGrade,
    deleteGrade
};
