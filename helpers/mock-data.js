const users = [
    {
        id: '1',
        firstName: 'John',
        lastName: 'Doe',
        email: 'john.doe@example.com',
        age: 30,
        gender: 'male'
    },
    {
        id: '2',
        firstName: 'Jane',
        lastName: 'Doe',
        email: 'jane.doe@example.com',
        age: 25,
        gender: 'female'
    },
    ];

const grades = [
    {
        id: '1',
        studentName: 'John Doe',
        group: 'A',
        subject: 'Math',
        examTicketNumber: '12345',
        grade: 'A',
        teacher: 'Mr. Smith'
    },
    {
        id: '2',
        studentName: 'Jane Doe',
        group: 'B',
        subject: 'Science',
        examTicketNumber: '54321',
        grade: 'B+',
        teacher: 'Ms. Johnson'
    },
    ];

module.exports = {
    users,
    grades,
};