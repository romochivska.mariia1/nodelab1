require('dotenv').config();

const config = {
    port: process.env.PORT || 3018, 
};

module.exports = config;