const gradeService = require('../services/grades.service');

async function createGrade(req, res) {
    try {
       const newGrade = await gradeService.createGrade(req.body);

        res.status(200).json({
            status: 200,
            data: newGrade,
        });
    } catch (err) {
        console.error(err);
        res.status(500).json({
            status: 500,
            error: err,
        });
    }
};

async function getGrades(req, res) {
    try {
        const grades = await gradeService.getGrades();
        res.status(200).json({
            status: 200,
            data: grades,
        });
    } catch (err) {
        console.error(err);
        res.status(500).json({
            status: 500,
            error: err,
        });
    }
};

async function getGrade(req, res) {
    try {
        const { gradeId } = req.params;
        const grade = await gradeService.getGradeById(gradeId);

        if (!grade) {
            return res.status(400).json({
                status: 400,
                message: 'Grade not found.',
            });
        }

        res.status(200).json({
            status: 200,
            data: grade,
        });
    } catch (err) {
        console.error(err);
        res.status(500).json({
            status: 500,
            error: err,
        });
    }
};

async function updateGrade(req, res) {
    try {
        const { gradeId } = req.params;
        const updatedData = req.body;
        await gradeService.updateGrade(gradeId, updatedData);

        res.status(200).json({
            status: 200,
        });
    } catch (err) {
        console.error(err);
        res.status(500).json({
            status: 500,
            error: err,
        });
    }
};

async function deleteGrade(req, res) {
    try {
        const { gradeId } = req.params;
        await gradeService.deleteGrade(gradeId);

        res.status(200).json({
            status: 200,
        });
    } catch (err) {
        console.error(err);
        res.status(500).json({
            status: 500,
            error: err,
        });
    }
};

module.exports = {
    createGrade,
    getGrades,
    getGrade,
    updateGrade,
    deleteGrade,
};
